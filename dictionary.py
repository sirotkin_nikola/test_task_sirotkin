class Dictionary:
    def __init__(self, words):
        self.words = words

    def find_most_similar(self, term):
        min_changes = float('inf')
        most_similar_word = ''

        for word in self.words:
            changes = self.minimum_changes(term, word)

            if changes < min_changes:
                min_changes = changes
                most_similar_word = word

        return most_similar_word

    def minimum_changes(self, word1, word2):
        m = len(word1)
        n = len(word2)

        dp = [[0] * (n + 1) for _ in range(m + 1)]

        for i in range(m + 1):
            dp[i][0] = i

        for j in range(n + 1):
            dp[0][j] = j

        for i in range(1, m + 1):
            for j in range(1, n + 1):
                if word1[i - 1] == word2[j - 1]:
                    dp[i][j] = dp[i - 1][j - 1]
                else:
                    dp[i][j] = 1 + min(dp[i - 1][j - 1], dp[i - 1][j], dp[i][j - 1])

        return dp[m][n]

    def most_similar_word(self, term):
        return self.find_most_similar(term)