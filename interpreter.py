class Interpreter:
    def __init__(self):
        self.variables = {}

    def input(self, expression):
        expression = expression.strip()
        if not expression:
            return ''

        # Check if the expression is an assignment
        if '=' in expression:
            identifier, value_expression = map(str.strip, expression.split('='))
            value = self.evaluate_expression(value_expression)
            self.variables[identifier] = value
            return value

        # Evaluate the expression
        return self.evaluate_expression(expression)

    def evaluate_expression(self, expression):
        if expression.isnumeric():  # Check if the expression is a number
            return int(expression)
        elif expression in self.variables:  # Check if the expression is an identifier
            return self.variables[expression]
        elif '+' in expression:  # Check if the expression is an addition
            left_expression, right_expression = map(str.strip, expression.split('+'))
            return self.evaluate_expression(left_expression) + self.evaluate_expression(right_expression)
        elif '-' in expression:  # Check if the expression is a subtraction
            left_expression, right_expression = map(str.strip, expression.split('-'))
            return self.evaluate_expression(left_expression) - self.evaluate_expression(right_expression)
        elif '*' in expression:  # Check if the expression is a multiplication
            left_expression, right_expression = map(str.strip, expression.split('*'))
            return self.evaluate_expression(left_expression) * self.evaluate_expression(right_expression)
        elif '/' in expression:  # Check if the expression is a division
            left_expression, right_expression = map(str.strip, expression.split('/'))
            return self.evaluate_expression(left_expression) / self.evaluate_expression(right_expression)
        elif '%' in expression:  # Check if the expression is a modulo
            left_expression, right_expression = map(str.strip, expression.split('%'))
            return self.evaluate_expression(left_expression) % self.evaluate_expression(right_expression)
        else:
            raise ValueError("Invalid expression")